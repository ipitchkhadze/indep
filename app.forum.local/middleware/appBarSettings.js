export default ({ route, store }) => {
  // Take the last value (latest route child)
  const settings = route.meta.reduce((appBarSettings, meta) => meta.appBarSettings || appBarSettings, {
    showSearch: true,
    showBtnMiniVariant: true,
    showBtnFullscreen: true
  })
  store.commit('app/setAppBarSettings', { ...store.state.app.appBarSettings, ...settings })
}
