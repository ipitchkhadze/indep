export const formEntityMixin = {
  props: {
    itemId: Number
  },
  data () {
    return {
      loading: false,
      metaInfo: {
        title: undefined
      }
    }
  },
  methods: {
    close () {
      this.$router.push(this.entityPathURL)
    },
    async checkingChangesForm (options) {
      const params = {
        message: 'На странице есть несохраненные изменения. Продолжить?',
        okText: 'Продолжить',
        cancelText: 'Вернуться к редактированию',
        ...options
      }
      if (!_.isEqual(this.form, this.default)) {
        return await this.$dialog.warning({
          icon: 'mdi-alert',
          text: params.message,
          title: 'Внимание!',
          actions: {
            false: params.cancelText,
            true: {
              color: 'success',
              text: params.okText
            }
          }
        })
      } else {
        return true
      }
    },
    process () {
      console.log(this.itemId);
      this.itemId ? this.edit() : this.create()
    },
    async edit () {
      this.loading = true
      await this.$api.$patch(`${this.entityPathAPI}/${this.form.id}`, this.form)
        .then((res) => {
          this.updateForm(res.data)
          this.setMetaInfo()
        })
        .finally(() => {
          this.loading = false
        })
    },
    async create () {
      this.loading = true
      await this.$api.$post(`${this.entityPathAPI}`, this.form)
        .then((res) => {
          this.$router.push(`${this.entityPathURL}/${res.data.id}`)
        })
        .finally(() => {
          this.loading = false
        })
    },
    // eslint-disable-next-line require-await
    async addFetch () {
      return true
    },
    setMetaInfo () {
      // Задаем мета информацию
      this.metaInfo.title = this.form.name
    },
    async refresh () {
      const res = await this.checkingChangesForm()
      if (res) {
        await this.fetchData()
      }
    },
    async fetchData () {
      this.loading = true
      if (this.itemId) {
        const newValueForm = await this.$api.$get(`${this.entityPathAPI}/${this.itemId}`).then(res => res.data)
        this.updateForm(newValueForm)
        this.setMetaInfo()
      } else {
        this.form = this.default
      }
      await this.addFetch()
      this.loading = false
    },

    changeFieldItemDialog (field, value, stringAction, updateFullForm) {
      this.$dialog.warning({
        icon: 'mdi-alert',
        text: `Вы уверены, что хотите ${stringAction}?`,
        title: 'Внимание',
        actions: {
          false: 'Нет',
          true: {
            color: 'red',
            text: 'Да',
            handle: () => {
              this.changeFieldItem(field, value, updateFullForm)
            }
          }
        }
      })
    },
    async changeFieldItem (field, value, updateFullForm) {
      this.loading = true
      const form = updateFullForm ? {
        ...this.form,
        [field]: value
      } : {
        [field]: value
      }
      await this.$api.$patch(`${this.entityPathAPI}/${this.itemId}`, form).then((res) => {
        this.updateForm(res.data)
      })
      this.loading = false
    },
    updateForm (newValue) {
      this.default = _.cloneDeep(newValue)
      this.form = _.cloneDeep(newValue)
    }
  },
  async fetch () {
    await this.fetchData()
  },
  head () {
    return {
      title: this.metaInfo.title
    }
  }
}
