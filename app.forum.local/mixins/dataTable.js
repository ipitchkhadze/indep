export const dataTableMixin = {
  data () {
    return {
      totalDtItems: 0,
      dtOptions: {
        page: 1,
        itemsPerPage: 100
      },
      dtFilters: {},
      dtFooterProps: {
        itemsPerPageOptions: [5, 10, 20, 50, 100]
      },
      dtItems: [],
      loading: true,
      singularTitleAccusative: 'запись',
      sourceCancelToken: this.$api.CancelToken.source()
    }
  },
  watch: {
    dtOptions: {
      handler () {
        this.refresh()
      },
      deep: true
    },
    dtFilters: {
      handler () {
        this.refresh()
      },
      deep: true
    }
  },
  methods: {
    refresh () {
      this.loading = true
      this.sourceCancelToken.cancel('Operation canceled by the user.') // Отменяем отправленные request'ы
      this.sourceCancelToken = this.$api.CancelToken.source() // Задаем новый токен
      this.getItems().finally(() => {
        this.loading = false
      })
    },
    filterProcessing (filters) {
      let f = ''
      for (const filter in filters) {
        const val = filters[filter]
        if (val) {
          const [field, type] = filter.split(':')

          if (!type) {
            f += `&${field}=${val}`
          } else if (type === 'range') {
            const from = this.$moment(val.start).format('YYYY-MM-DD')
            const to = this.$moment(val.end).format('YYYY-MM-DD')
            f += `&${field}:range=${from} -- ${to}`
          } else {
            f += `&${field}:${type}=${val}`
          }
        }
      }
      return f
    },
    async getItems () {
      const f = this.filterProcessing(this.dtFilters)

      const { sortBy, sortDesc, page, itemsPerPage } = this.dtOptions

      await this.$api.$get(this.dtUrl + `?sortBy=${sortBy}&sortDesc=${sortDesc}&itemsPerPage=${itemsPerPage}&paginate=true&page=${page}${f}`, {
        cancelToken: this.sourceCancelToken.token
      }).then((resp) => {
        if (resp.collection) {
          this.dtItems = resp.data
          this.totalDtItems = resp.meta.total
        } else {
          this.dtItems = resp.data.data
          this.totalDtItems = resp.data.total
        }
      })
        .catch((error) => {
          // Если ошибка не связана с отменой запроса
          if (!this.$axios.isCancel(error)) {
            // handle error
            this.$dialog.notify.error(error.response.data.message, {
              position: 'bottom-right',
              timeout: 5000
            })
          }
        })
    },
    editItem (item) {
      this.$router.push(`${this.entityPathURL}/${item.id}`)
    },
    deleteItemDialog (item) {
      this.$dialog.error({
        icon: 'mdi-alert',
        text: `Вы действительно хотите удалить ${this.singularTitleAccusative}?`,
        title: 'Внимание',
        actions: {
          false: 'Нет',
          true: {
            color: 'red',
            text: 'Да',
            handle: () => {
              this.deleteItem(item)
            }
          }
        }
      })
    },
    async deleteItem (item) {
      this.loading = true
      const index = this.dtItems.indexOf(item)
      await this.$api.$delete(`${this.dtUrl}/${item.id}`)
        .then((res) => {
          this.dtItems.splice(index, 1)
        })
      this.loading = false
    },
    changeActiveItemDialog (item) {
      this.$dialog.warning({
        icon: 'mdi-alert',
        text: `Вы действительно хотите ${item.active ? 'деактивировать' : 'активировать'} ${this.singularTitleAccusative}?`,
        title: 'Внимание',
        actions: {
          false: 'Нет',
          true: {
            color: 'red',
            text: 'Да',
            handle: () => {
              this.changeActiveItem(item)
            }
          }
        }
      })
    },
    async changeActiveItem (item) {
      this.loading = true
      await this.$api.$patch(`${this.dtUrl}/${item.id}/activate`, { active: !item.active })
        .then(() => {
          this.refresh()
        })
      this.loading = false
    }
  }
}
