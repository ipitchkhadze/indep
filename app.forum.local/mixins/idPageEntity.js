export const idPageEntityMixin = {
  beforeRouteLeave (to, from, next) {
    // вызывается перед переходом от пути, соответствующего текущему компоненту;
    // имеет доступ к контексту экземпляра компонента `this`.
    this.$refs.form.checkingChangesForm().then((res) => {
      if (res) {
        next()
      }
    })
  }
}
