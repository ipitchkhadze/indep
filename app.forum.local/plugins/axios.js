import Vue from 'vue'

export default function ({
  app,
  $axios,
  redirect
}, inject) {
  const uuidv4 = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = Math.random() * 16 | 0
      const v = c === 'x' ? r : (r & 0x3 | 0x8)
      return v.toString(16)
    })
  }

  // Create a custom axios instance
  const api = $axios.create({
    headers: {
      common: {
        Accept: 'application/json, */*',
      }
    }
  })

  // Set baseURL to something different
  api.setBaseURL(process.env.NUXT_ENV_API_URL)

  api.interceptors.request.use(
    (config) => {
      config.headers['Idempotency-Key'] = uuidv4()
      return config
    },
    (error) => {
      return Promise.reject(error)
    }
  )

  // Inject to context as $api
  inject('api', api)

  // Add context $api to $dialog
  app.context.$dialog.context.$api = api
  // or
  // Vue.prototype.$dialog.context.$api = api

  const types = [$axios, api]
  types.forEach((element) => {
    element.onError((error) => {
      const code = parseInt(error.response && error.response.status)
      // if (code === 400) {
      //   redirect('/400')
      // }
      if (code === 404) {
        redirect('/error/not-found')
      } else if (code === 500) {
        redirect('/error/unexpected')
      }
      if (!(error.response.config.url === `${process.env.NUXT_ENV_BACK_URL}/api/user` && code === 401)) {
        const listErrorCodes = [422, 401, 403, 500]
        if (listErrorCodes.find(i => i === code)) {
          let message = ''
          if (error.response.data.errors) {
            message = '<ul class="pl-3">'
            Object.keys(error.response.data.errors).map(function (objectKey, index) {
              message += '<li>' + error.response.data.errors[objectKey] + '</li>'
            })
            message += '</ul>'
          } else {
            message = error.response.data.message
          }

          Vue.prototype.$dialog.notify.error(message, {
            position: 'bottom-right',
            timeout: 5000
          })
        }
      }
    })

    element.onResponse((response) => {
      const { config } = response
      const listMethods = ['post', 'patch', 'delete']
      if ((listMethods.find(i => i === config.method)) && (response.data.message)) {
        Vue.prototype.$dialog.notify.success(response.data.message, {
          position: 'bottom-right',
          timeout: 5000
        })
      }
    })
  })
}
