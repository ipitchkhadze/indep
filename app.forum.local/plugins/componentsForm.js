import Vue from 'vue'

import KVTextField from '@/components/form/KVTextField'
import KVSelect from '@/components/form/KVSelect'
import KVCheckbox from '@/components/form/KVCheckbox'
import KVSwitch from '@/components/form/KVSwitch'
import KVImageUploader from '@/components/form/KVImageUploader'
import KVAutocomplete from '@/components/form/KVAutocomplete'
import KVTextarea from '@/components/form/KVTextarea'
import KVCurrencyField from '@/components/form/KVCurrencyField'
import KVIconColorPicker from '@/components/form/KVIconColorPicker'
import KVFileUploader from '@/components/form/KVFileUploader'

import VVDateTimePicker from '@/components/form/VVDateTimePicker' // ToDo: временный DTPicker. Заменить или перебрать

Vue.component('KVTextField', KVTextField)
Vue.component('KVSelect', KVSelect)
Vue.component('KVCheckbox', KVCheckbox)
Vue.component('KVSwitch', KVSwitch)
Vue.component('KVImageUploader', KVImageUploader)
Vue.component('KVAutocomplete', KVAutocomplete)
Vue.component('KVTextarea', KVTextarea)
Vue.component('KVCurrencyField', KVCurrencyField)
Vue.component('VVDateTimePicker', VVDateTimePicker)
Vue.component('KVIconColorPicker', KVIconColorPicker)
Vue.component('KVFileUploader', KVFileUploader)
