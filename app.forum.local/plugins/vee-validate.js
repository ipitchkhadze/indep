import Vue from 'vue'

import {
  extend,
  ValidationObserver,
  ValidationProvider,
  localize
} from 'vee-validate'

import * as rules from 'vee-validate/dist/rules'
//
// extend('required_if_not', {
//   ...rules.required_if,
//   validate: (value, args) => {
//     let target_value = args.target;
//     return Boolean(target_value || value);
//   },
//   message: (fieldName, placeholders) => {
//     return `Поле ${fieldName} обязательно, если не заполнено поле ${placeholders.target}`;
//   }
// });

import ru from 'vee-validate/dist/locale/ru.json'

const MOBILEREG = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/

// extend('phoneOrEmail', {
//   validate: value => {
//     return rules.email.validate(value) || MOBILEREG.test(value);
//   },
//   message: 'Поле {_field_} должно быть телефоном или email'
// });
//
extend('phone', {
  validate: (value) => {
    return MOBILEREG.test(value)
  },
  message: 'Поле {_field_} должно быть валидным номером телефона'
})

Object.keys(rules).forEach((rule) => {
  // eslint-disable-next-line import/namespace
  extend(rule, rules[rule])
})

// Install and Activate the Russian locale.
localize('ru', ru)

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
