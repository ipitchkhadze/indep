export default [
  {
    icon: 'mdi-account-multiple',
    key: 'menu.users',
    text: 'Пользователи',
    items: [
      {
        text: 'Список',
        link: '/users'
      }
    ]
  }
]
