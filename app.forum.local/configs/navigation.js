import menuDirectories from './menus/directories.menu'

export default {
  // main navigation - side menu
  menu: [
    {
      text: '',
      key: '',
      items: [
        {
          icon: 'mdi-home',
          key: 'menu.home',
          text: 'Главная',
          link: '/'
        }
      ]
    },
    {
      text: 'Directories',
      key: 'menu.directories',
      items: menuDirectories
    }
  ]
}
