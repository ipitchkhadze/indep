import time from './time'
import theme from './theme'
import toolbar from './toolbar'
import locales from './locales'
import currencies from './currencies'
import navigation from './navigation'

export default {
  // product display information
  product: {
    name: 'Forum',
    version: '1.0.0'
  },

  // google maps
  // maps,

  // time configs
  time,

  // icon libraries
  // icons,

  // theme configs
  theme,

  // toolbar configs
  toolbar,

  // locales configs
  locales,

  // analytics configs
  // analytics,

  // currencies configs
  currencies,

  // navigation configs
  navigation
}
