import configs from '../configs'

const { product, time, theme, currencies } = configs

const { globalTheme, menuTheme, toolbarTheme, isToolbarDetached, isContentBoxed, isRTL } = theme
const { currency, availableCurrencies } = currencies

export const state = () => ({
  product,

  time,

  // currency
  currency,
  availableCurrencies,

  // themes and layout configurations
  globalTheme,
  menuTheme,
  toolbarTheme,
  isToolbarDetached,
  isContentBoxed,
  isRTL,

  storageUrl: `${process.env.NUXT_ENV_BACK_URL}/storage`,

  appBarSettings: null
})

export const mutations = {
  /**
   * Theme and Layout
   */
  setGlobalTheme: (state, theme) => {
    // Строка не работает из-за отсутствия контекста Vuetify. Делать смену в компоненте, вызывающем эту мутацию
    // Vuetify.framework.theme.dark = theme === 'dark'
    state.globalTheme = theme
  },
  setRTL: (state, isRTL) => {
    // Строка не работает из-за отсутствия контекста Vuetify. Делать смену в компоненте, вызывающем эту мутацию
    // Vuetify.framework.rtl = isRTL
    state.isRTL = isRTL
  },
  setContentBoxed: (state, isBoxed) => {
    state.isContentBoxed = isBoxed
  },
  setMenuTheme: (state, theme) => {
    state.menuTheme = theme
  },
  setToolbarTheme: (state, theme) => {
    state.toolbarTheme = theme
  },
  setTimeZone: (state, zone) => {
    state.time.zone = zone
  },
  setTimeFormat: (state, format) => {
    state.time.format = format
  },
  setCurrency: (state, currency) => {
    state.currency = currency
  },
  setToolbarDetached: (state, isDetached) => {
    state.isToolbarDetached = isDetached
  },
  setAppBarSettings: (state, settings) => {
    state.appBarSettings = settings
  }
}

export const actions = {

}
