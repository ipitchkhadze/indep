import webpack from 'webpack'

import ru from 'vuetify/lib/locale/ru'
import en from 'vuetify/lib/locale/en'

import config from './configs'

export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  // The server Property (https://nuxtjs.org/guides/configuration-glossary/configuration-server)
  server: {
    port: process.env.SERVER_PORT || 3000, // default: 3000
    host: process.env.SERVER_HOST || 'localhost' // default: localhost,
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - forum',
    title: 'Главная',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'preconnect',
        href: 'https://fonts.gstatic.com'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Fira+Sans+Condensed:ital,wght@0,100;0,300;0,400;0,500;0,600;0,700;1,100;1,300;1,400;1,500;1,600;1,700&display=swap'
      }
    ],
    script: []
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '~/assets/scss/vuetify/overrides',
    '~/assets/scss/transitions.scss',
    '~/assets/scss/add.scss',
    'animate.css/animate.min.css'
  ],

  // Router settings
  router: {
    middleware: ['appBarSettings']
  },

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~/plugins/vue-events',
    '~/plugins/helpers',
    '~/plugins/axios',
    '~/plugins/vue-i18n.js',
    '~/plugins/vue-shortkey.js',
    '~/plugins/vee-validate.js',
    '~/plugins/apexcharts.js',
    '~/plugins/animate.js',
    '~/plugins/clipboard.js',
    '~/plugins/vuescroll.js',
    '~/plugins/componentsForm.js',
    '~/filters/formatCurrency.js',
    '~/filters/formatDate.js',
    '~/filters/textTransform.js'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    '@nuxtjs/auth-next',
    '@nuxtjs/vuetify',
    '@nuxtjs/moment'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    'vuetify-dialog/nuxt',
    '@nuxtjs/axios',
    ['v-currency-field/nuxt-treeshaking', {
      locale: 'ru-Latn',
      decimalLength: 2,
      autoDecimalMode: true,
      min: null,
      max: null,
      defaultValue: 0,
      valueAsInteger: false,
      allowNegative: false
    }],
    ['portal-vue/nuxt']
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.BASE_URL || 'http://localhost:8000',
    credentials: true
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/scss/vuetify/variables'],
    dark: config.theme.globalTheme === 'dark',
    customProperties: true,
    treeShake: true,
    rtl: config.theme.isRTL,
    theme: {
      options: {
        customProperties: true
      },
      themes: {
        dark: config.theme.dark,
        light: config.theme.light
      }
    },
    lang: {
      current: config.locales.locale,
      locales: {
        ru: {
          ...ru,
          dataIterator: { loadingText: 'Загрузка... Пожалуйста, подождите' }
        },
        en
      }
      // To use Vuetify own translations without Vue-i18n comment the next line
      // t: (key, ...params) => this.i18n.t(key, params)
    }
  },
  auth: {
    strategies: {
      cookie: {
        cookie: {
          name: 'XSRF-TOKEN'
        }
      },
      laravelSanctum: {
        endpoints: {
          login: {
            url: '/login',
            method: 'post'
          },
          logout: {
            url: '/logout',
            method: 'post'
          },
          user: {
            headers: {
              'Idempotency-Key': 'kajhsgkjhasgdkjhasgdkjashgd'
            },
            url: '/api/v1/auth/user',
            method: 'get'
          }
        },
        provider: 'laravel/sanctum',
        url: process.env.BASE_URL || 'http://localhost:8000'
      }
    },
    redirect: {
      login: '/auth/signin',
      logout: '/auth/signin',
      callback: '/auth/signin',
      home: '/'
    }
  },
  // Moment Configuration (https://github.com/nuxt-community/moment-module)
  moment: {
    defaultLocale: 'ru',
    locales: ['ru'],
    timezone: true,
    defaultTimezone: config.time.zone
  },
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        _: 'lodash'
      })
    ]
  }
}
