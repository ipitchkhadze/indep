<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('App\Http\Controllers\Api\V1')->prefix('v1')
    ->group(function () {

        Route::middleware('auth:sanctum')->group(function () {

            Route::namespace('Auth')->prefix('auth')->group(function () {
                Route::get('user', 'UserController@user');
            });
            Route::apiResources([
                'users'          => 'UserController',
                //{{resource}}
            ]);
        });
    });
