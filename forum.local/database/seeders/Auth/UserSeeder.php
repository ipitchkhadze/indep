<?php

namespace Database\Seeders\Auth;

use App\Models\Role;
use App\Models\User;
use Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name'           => 'Администратор',
                'login'          => 'sysadmin',
                'email'          => 'sysadmin@forum.com',
                'password'       => Hash::make('password'),
                'remember_token' => Str::random(10),
            ],

        ];

        foreach ($users as $user) {
            $userModel = User::updateOrCreate(['login' => $user['login']], $user);
        }
    }
}
