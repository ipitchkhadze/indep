<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'login',
        'email',
        'phone',
        'active',
        'mfa',
        'password',
        'image',
        'position'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'        => 'datetime:Y-m-d H:i:s',
        'updated_at'        => 'datetime:Y-m-d H:i:s',
        'active'            => 'boolean',
        'mfa'               => 'boolean',
        'email_verified_at' => 'datetime',
    ];


    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class, 'contractor_id');
    }

    public function acts()
    {
        return $this->hasManyThrough(Act::class, Contract::class, 'contractor_id');
    }

    public function scopeContractor($query)
    {
        return $query->whereHas('roles', function ($q) {
            $q->where('code', 'contractor');
        });
    }

    public function hasRoles($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('code', $role);
        }

        return !!$role->intersect($this->roles)->count();
    }

    public function getScopesAttribute()
    {
        $sc = [];
        foreach ($this->roles as $role) {
            $sc[] = 'role:' . $role->code;
        }

        return $sc;
    }

    public function getRolesNamesAttribute()
    {
        return ($this->roles) ? $this->roles->pluck('code') : [];
    }

    public function getRolesIdsAttribute()
    {
        return ($this->roles) ? $this->roles->pluck('id') : [];
    }
}
