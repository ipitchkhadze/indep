<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? '';
        return [
            'id'       => 'integer|nullable',
            'name'     => 'string|required',
            'email'    => 'string|nullable|unique:users,email' . $id,
            'login'    => 'string|required|unique:users,login' . $id,
            'password' => 'string|nullable'
        ];
    }

    public function messages()
    {
        return [
            'login.unique' => 'Пользователь с таким логином уже существует в системе',
            'login.email'  => 'Пользователь с такой почтой уже существует в системе',
        ];
    }
}

