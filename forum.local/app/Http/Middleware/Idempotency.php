<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Cache;

class Idempotency
{

    const IDEMPOTENCY_HEADER     = 'Idempotency-Key';
    const IDEMPOTENCY_EXPIRATION = 1; // in minutes

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasHeader(self::IDEMPOTENCY_HEADER)) {
            $requestID = $request->header(self::IDEMPOTENCY_HEADER);

            if (!Cache::has($requestID) || env('APP_ENV') == "local") {

                $response = $next($request);
                $response->header(self::IDEMPOTENCY_HEADER, $requestID);
                $response->header("Is-Replay", "false");


                if ($response->status() == 200)
                    Cache::put($requestID, [
                        "response" => $response->getContent(),
                        "path"     => $request->path(),
                        "method"   => $request->getMethod(),
                        "status"   => $response->status()
                    ], Carbon::now()->addMinutes(self::IDEMPOTENCY_EXPIRATION));
                return $response;
            } else {

                $responseFromCache = Cache::get($requestID);
                if ($request->path() == $responseFromCache["path"] && $request->getMethod() == $responseFromCache['method']) {
                    return response($responseFromCache["response"], $responseFromCache["status"])->withHeaders([
                        self::IDEMPOTENCY_HEADER => $requestID,
                        "Is-Replay"              => "true",
                        "Content-Type"           => "application/json"
                    ]);
                } else {

                    return response(
                        [
                            "message" => self::IDEMPOTENCY_HEADER . " повторное использование ключа в разных запросах"
                        ], 400);
                }

            }

        } else {

            return response(
                [
                    "message" => "Заголовок " . self::IDEMPOTENCY_HEADER . " не присутствует в запросе."
                ], 400);
        }
    }

}
