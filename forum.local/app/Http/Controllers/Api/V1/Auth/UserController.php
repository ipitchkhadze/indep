<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @group User
     *
     * Информация о текущем пользователе
     */
    public function user(Request $request)
    {
        $user        = $request->user();
        return $this->sendResponse($user,'Пользователь.');
    }

}
