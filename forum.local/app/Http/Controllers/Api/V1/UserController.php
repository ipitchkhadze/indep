<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

/**
 * Class UserController
 * @group User
 * @package App\Http\Controllers\Api\V1
 */
class UserController extends Controller
{
    /**
     *
     * Список
     *
     * @param Request $request
     */
    public function index(Request $request)
    {
        $users = new User();

        $users = ($request->has('paginate')) ? $users->paginate(
            $request->has('itemsPerPage') ? intval($request->get('itemsPerPage')) : intval(config('settings.paginate'))
        ) : $users->get();
        return UserCollection::make($users);
    }

    /**
     *
     * Создание
     *
     * @param UserRequest $request
     * @return JsonResponse
     */
    public function store(UserRequest $request)
    {
        $user = User::create(
            $request->validated()
        );
        return $this->sendResponse(UserResource::make($user), 'Запись создана');
    }

    /**
     *
     * Одина сущность
     *
     * @param User $user
     * @return JsonResponse
     */
    public function show(User $user)
    {
        return $this->sendResponse(UserResource::make($user), 'Запись');
    }

    /**
     *
     * Обновление
     *
     * @param UserRequest $request
     * @param User $user
     * @return JsonResponse
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update(
            $request->validated()
        );

        return $this->sendResponse(UserResource::make($user), 'Запись обновлена');
    }

    /**
     *
     * Удаление
     *
     * @param User $user
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return $this->sendResponse('', 'Запись удалена');
    }
}
